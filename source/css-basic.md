---
title: CSSの基本
abbrlink: 91d161ac
date: 2017-08-07 00:57:55
type: guide
order: 2
tags:
---
# 要素と属性

以下の `html` において、 `a` を要素と呼び、 `href` や `disabled` を属性と呼びます。

```html
<a href="http://example.org" disabled>example.org</a>
```

`disabled` は

# プロパティーについて

CSSは以下のような構造をもつ。

{% codeblock 'CSSの構造' lang:css %}
.selector { property: value }
{% endcodeblock %}

より具体的な例を出すと、

{% codeblock green.css lang:css %}
.green { color: green; }
{% endcodeblock %}

これは、greenという名前のクラスに対して色(color)を緑(green)に指定するものである。
つまり、次のようなHTMLに対して、

<p data-height="265" data-theme-id="dark" data-slug-hash="VzPzqb" data-default-tab="html,result" data-user="9re" data-embed-version="2" data-pen-title="VzPzqb" class="codepen">See the Pen <a href="https://codepen.io/9re/pen/VzPzqb/">VzPzqb</a> by 9re (<a href="https://codepen.io/9re">@9re</a>) on <a href="https://codepen.io">CodePen</a>.</p>
<script async src="https://production-assets.codepen.io/assets/embed/ei.js"></script>

このようにスタイルの指定(この場合は、色の指定)ができる。

# プロパティの種類

代表的なものを以下に示す。より詳細なリストに関しては、[MDN CSS reference](https://developer.mozilla.org/en-US/docs/Web/CSS/Reference)を参照のこと。
ここでは、個々の意味は説明しないが、概ね以下のようなものがあり、かつ一部のプロパティに関しては「それが有効になる為の条件がある」ということだけ覚えておく。

property|意味
---|---
background|背景
background-color|背景色
background-image|背景画像
background-position|背景画像の位置
background-repeat|背景の繰り返し
border|ボーダー
border-color|ボーダーの色
border-style|ボーダーの種類
border-width|ボーダーの幅
color|文字色
cursor|カーソルの種類
display|表示モデル
font|フォント
font-family|フォントの種類
font-size|フォントの大きさ
font-weight|フォントの太さ
height|高さ
left|左からの位置
line-height|行の高さ
margin|マージン
overflow|要素がはみ出した場合の表示方法の指定
padding|パディング
position|位置の決め方
float|文字の回り込みの指定
text-align|文字寄せ
text-decoration|文字の装飾(下線等)
top|上からの位置
vertical-align|縦方向の寄せ方について
visibility|表示するかどうか
width|幅
z-index|z方向の重なりの優先度

# セレクターについて

## 要素に対するセレクター

## タイプセレクタ

要素に対するセレクタです。以下のCSSでは、全ての aタグの下線を非表示にします。

```css
a { text-decoration: none }
```

## クラスセレクタ

要素の `class` 属性にマッチさせるためのCSSのセレクタをクラスセレクタと呼びます。 `.`から始まります。

## IDセレクタ

要素の `id` 属性にマッチさせるためのCSSのセレクタをIDセレクタと呼びます。`#` から始まります。要素のIDは htmlドキュメント内で
一個のユニークなものである必要があります。

### 例

以下のhtmlは

```html
<p class="key" id="principal">
```

`key` という名前のクラス属性と `principal` というID属性を持つため、

```css
.key {
  color: green;
}

#principal {
  font-weight: bolder;
}
```

というクラスセレクタ、IDセレクタに対するCSSがあった場合、それぞれがマッチする為これらのCSSが適用されます。

## 属性セレクタ

セレクタは、2 個の特別な属性 `class` と `id` 以外のものも指定できます。角括弧を使用して 他の属性 を指定できます。括弧内に属性名を書き、オプションとしてマッチ演算子と値を続けて書きます。

|セレクタ|意味|
|--:|:--|
|[disabled]|"disabled" 属性を持つすべての要素を選択します。|
|[type='button']|"type" 属性の値に "button" を持つ要素を選択します。|

## 疑似クラスセレクタ

要素だけでなく、ナビゲータの履歴 (例えば `:visited` ) やコンテンツの状態 (フォーム要素の `:checked` )、マウスの位置 (`:hover` は要素上にマウスポインタがあるかどうか知ることができます) など、関連する外部因子にもスタイルを適用できます。

### 擬似クラスの一覧

* [:link](https://developer.mozilla.org/ja/docs/Web/CSS/:link)
* [:visited](https://developer.mozilla.org/ja/docs/Web/CSS/:visited)
* [:active](https://developer.mozilla.org/ja/docs/Web/CSS/:active)
* [:hover](https://developer.mozilla.org/ja/docs/Web/CSS/:hover)
* [:focus](https://developer.mozilla.org/ja/docs/Web/CSS/:focus)
* [:first-child](https://developer.mozilla.org/ja/docs/Web/CSS/:first-child)
* [:last-child](https://developer.mozilla.org/ja/docs/Web/CSS/:last-child)
* [:nth-child](https://developer.mozilla.org/ja/docs/Web/CSS/:nth-child)
* [:nth-last-child](https://developer.mozilla.org/ja/docs/Web/CSS/:nth-last-child)
* [:nth-of-type](https://developer.mozilla.org/ja/docs/Web/CSS/:nth-of-type)
* [:first-of-type](https://developer.mozilla.org/ja/docs/Web/CSS/:first-of-type)
* [:last-of-type](https://developer.mozilla.org/ja/docs/Web/CSS/:last-of-type)
* [:empty](https://developer.mozilla.org/ja/docs/Web/CSS/:empty)
* [:target](https://developer.mozilla.org/ja/docs/Web/CSS/:target)
* [:checked](https://developer.mozilla.org/ja/docs/Web/CSS/:checked)
* [:enabled](https://developer.mozilla.org/ja/docs/Web/CSS/:enabled)
* [:disabled](https://developer.mozilla.org/ja/docs/Web/CSS/:disabled)

## 疑似要素

|セレクタ|説明|
|--:|:--|
|::after|要素の仮想的な最後の子要素となり疑似要素を生成|
|::before|要素の最初の子要素となる擬似要素を生成|
|::selection|ユーザーが (マウスなどで) ハイライトした文書部分にスタイルを適用|

# CSSの優先度

## 詳細度について

[https://www.w3.org/TR/css3-selectors/#specificity](https://www.w3.org/TR/css3-selectors/#specificity)

|セレクター|ランク|
|--:|:--|
|ID|A|
|クラス、属性セレクター、擬似クラス|B|
|要素、疑似要素|C|

上記セレクターのランク毎にカウントし、A-B-Cの順で比較することで詳細度の強さが決まり、詳細度の高いCSSの方が優先されます。

例

|セレクター|詳細度|
|--:|:--|
|li|0-0-1|
|ul li|0-0-2|
|ul ol li.red|0-1-3|
|li.red.level|0-2-1|
|#main|1-0-0|
|#main .container|1-1-0|

例外として `!important` のついたCSS

```css
.hidden {
  visibility: hidden !important;
}
```

やインラインの `style` タグのCSS

```html
<div style="background-color: red"></div>```

の方が優先されるというのがありますが、これらは自分で使うのは極力さけ、
他人の書いたコーディングで見かけたときにだけ思い出すようにしておきます。


