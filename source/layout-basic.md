---
title: レイアウトの基本 
abbrlink: 91d161ac
date: 2017-08-07 00:57:55
type: guide
order: 3
tags:
---
レイアウトの基本として以下のポイントを覚えます。

* [ボックスモデル](#ボックスモデル)
* [display(block, inline)](#display)
* [position](#position)
* [z-index](#z-index)
* [float](#float)

## ボックスモデル

下図のように、 外側から`margin`, `border`, `padding`, `content`という名前がついています。
`background` (背景) は `padding` と `content` の領域をあわせた領域に対して適用されます。
`margin`の背景は常に透明で、`border`は `border-color`等によって色等を指定します。

![ボックスモデル](/images/box-model.png)

また、上下左右の領域についてそれぞれ別々にCSSをしてすることも可能です。

例)

```css
.list-container {
  margin-top: 12px;
  padding-top: 8px;
  padding-bottom: 12px;
}
```

### border-size

視覚的な見やすさ、装飾的な理由から `border` をつけることがありますが、`border` をつけると幅や高さが
`border-width` 分増えてしまい、CSSの値の計算が複雑になったり、各値が依存しあうことにより非常にメンテナンス性
の悪いものとなってしまうことがあります。このような時、`border-size: border-box`は非常に便利です。

![border-box](/images/border-box.png)

## display

### block

要素は親要素(コンテナ)の幅全体を占有して、<u>前後に改行</u>を伴って表示されます。

### inline

<u>要素の領域のみ</u>を占有し、行の中のどこでも存在することが可能です。

### inline-block

`inline` 同様、行の中のどこでも存在することが出来、<u>幅や高さの指定が出来ます</u>。

[サンプル](https://codepen.io/huijing/pen/PNMxXL)を開いてブラウザの幅を変更したりして
レイアウトがどう変わるか確認して見てください。

`inline-block` の最も良く使う使い方は、`float: left`と合わせて、グリッドレイアウトなどのように
横にブロックを並べる使い方です。

## position

### absolute

`top`, `left`, `right`, `bottom`等の値によって配置場所を決定します。この場合の要素に`absolute`
を指定した要素の大きさは含まれません。座標は、最も近い親要素のうち `position` が `static` でないものを起点とします。

### fixed

`absolute` 同様絶対座標によって位置を指定する方法ですが、スクロールに対しても位置が固定されます。

### relative

基本的に `absolute` で指定する要素の座標の起点となる親の要素に指定すると考えて下さい。単独で使うことはほぼありません。

<p data-height="265" data-theme-id="0" data-slug-hash="mqJNez" data-default-tab="css,result" data-user="9re" data-embed-version="2" data-pen-title="CSS: position" class="codepen">See the Pen <a href="https://codepen.io/9re/pen/mqJNez/">CSS: position</a> by 9re (<a href="https://codepen.io/9re">@9re</a>) on <a href="https://codepen.io">CodePen</a>.</p>
<script async src="https://production-assets.codepen.io/assets/embed/ei.js"></script>

## z-index

htmlに登場する順にオブジェクトは下から配置されますが、配置する上下の位置(画面に対して垂直な方向)を変更したい時に追加います。
主に[position: fixed](#fixed)と合わせてメニューが常に上に表示されているようなレイアウトが複数ある際に順序をしていします。

<p data-height="265" data-theme-id="0" data-slug-hash="dZoxaa" data-default-tab="css,result" data-user="9re" data-embed-version="2" data-pen-title="z-index" class="codepen">See the Pen <a href="https://codepen.io/9re/pen/dZoxaa/">z-index</a> by 9re (<a href="https://codepen.io/9re">@9re</a>) on <a href="https://codepen.io">CodePen</a>.</p>
<script async src="https://production-assets.codepen.io/assets/embed/ei.js"></script>

## float

フロートはレイアウトの回り込み指定のためのものですが、主な用途は以下の2つです。

### テキストの回り込み

<p data-height="265" data-theme-id="0" data-slug-hash="POPYbN" data-default-tab="html,result" data-user="9re" data-embed-version="2" data-pen-title="float" class="codepen">See the Pen <a href="https://codepen.io/9re/pen/POPYbN/">float</a> by 9re (<a href="https://codepen.io/9re">@9re</a>) on <a href="https://codepen.io">CodePen</a>.</p>
<script async src="https://production-assets.codepen.io/assets/embed/ei.js"></script>

### ボックスを横並びに並べる

<p data-height="265" data-theme-id="0" data-slug-hash="QObPJK" data-default-tab="css,result" data-user="9re" data-embed-version="2" data-pen-title="QObPJK" class="codepen">See the Pen <a href="https://codepen.io/9re/pen/QObPJK/">QObPJK</a> by 9re (<a href="https://codepen.io/9re">@9re</a>) on <a href="https://codepen.io">CodePen</a>.</p>
<script async src="https://production-assets.codepen.io/assets/embed/ei.js"></script>

## デフォルトのCSS

デフォルトで `padding` や `margin` がついていますが、一番重要なのはデフォルトの `display` です。

### ブロック要素

以下の要素はデフォルトが`display: block`です。

 address, article, aside, canvas, dd, div, dl, fieldset, figcaption
 figure, footer, form, h1, h2, h3, h4, h5, h6
 header, hgroup, hr, li, main, nav, noscript, ol
 output, p, pre, section, table, tfoot, ul, video

### インライン要素

以下の要素はデフォルトが`display: inline`です。

 b, big, i, small, tt
 abbr, acronym, cite, code, dfn, em, kbd, strong, samp, time, var
 a, bdo, br, img, map, object, q, script, span, sub, sup
 button, input, label, select, textarea

### 置換要素

以下の要素は置換要素といい、CSSの適用外です。より正確に言うと、外部ファイル等に置き換えられるため、CSSのフォーマットのモデルとは
異なる挙動を示します。
 
 img, iframe, video, embed, audio, canvas, object, applet
 
 以下のhtmlがもし置換要素でなかった場合、属性のない空タグであるため、高さ、幅は0の筈ですが、実際は、`apple.png`
 というhtml外のオブジェクトに置き換えられ、そのオブジェクトの高さや幅が適応されます。
 
```html
<img src='apple.png'/>
```
