---
title: HTML + CSS勉強会
abbrlink: cc2fdc4
date: 2017-08-07 00:07:40
type: guide
order: 1
tags:
---
## 目的、あるいは目標

* 細部には拘らず、概要を学ぶこと
* 分からないことが出てきた場合、検索などによって自己解決できる力を身につける。

## 歴史
HTMLはCERNのエンジニアTim Berners-Leeによって最初に提案された。

[世界で最初のWeb Page](http://info.cern.ch/hypertext/WWW/TheProject.html)

それはリンクと文章のスタイルという概念を持ったインタネットを前提としたマークアップ言語であった。
歴史的には最初スタイリングの為のタグとドキュメント要素の区別が無かった。
例えば、HTML4.01には以下のようなスタイリングの為だけのタグが存在する

b, i, center

その後CSSというスタイリング専用の言語が考案されるに至り、HTMLはセマンティック、CSSはデザインという分離が起きた。

## 概要

* [CSSの基本](/css-basic.html)
* [レイアウトの基本](/layout-basic.html)

